var app = {
	// Application Constructor
	initialize: function () {
		this.bindEvents();
	},
	// Bind Event Listeners
	//
	// Bind any events that are required on startup. Common events are:
	// 'load', 'deviceready', 'offline', and 'online'.
	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	// deviceready Event Handler
	//
	// The scope of 'this' is the event. In order to call the 'receivedEvent'
	// function, we must explicity call 'app.receivedEvent(...);'
	onDeviceReady: function () {
		//app.receivedEvent('deviceready');
        document.addEventListener("pause", onPause, false);
        document.addEventListener("resume", onResume, false);
        document.addEventListener("menubutton", onMenuKeyDown, false);
        document.addEventListener("searchbutton", onSearchKeyDown, false);
        $(document).bind("mobileinit", function () {
                $.support.cors                 = true;
                $.mobile.allowCrossDomainPages = true;
                $.mobile.pushStateEnabled      = true;
            app.addmenu();
        });
	},
	// Update DOM on a Received Event
	receivedEvent: function (id) {
		var parentElement = document.getElementById(id),
		    listeningElement = parentElement.querySelector('.listening'),
		    receivedElement = parentElement.querySelector('.received');

		listeningElement.setAttribute('style', 'display:none;');
		receivedElement.setAttribute('style', 'display:block;');

		console.log('Received Event: ' + id);
	},
	
	exit: function () {
		navigator.app.exitApp();
	},
	
    onPause: function () {
    	
    },
	
    onResume: function () {
    	
    },
	
    onMenuKeyDown: function () {
        $.mobile.navigate("index.html");
    },
	
    onSearchKeyDown: function () {
        $.mobile.navigate("consult.html");
    },
	
	xalert: function (message) {
		function alertDismissed() {
			console.log("Alert dismissed");
		}
        navigator.notification.vibrate(0);
        navigator.notification.alert(
			message,  // message
			alertDismissed,         // callback
			'MON RDV VISA',            // title
			'OK'                  // buttonName
		);
		navigator.notification.beep(2);
 
	},
	
	writeinfos: function (id, nom, prenom, telephone, email, npass, pexp, motif, depart, nbord, banq, assur, compass, drdv, hrdv, etat) {
		sessionStorage.setItem("id", id);
		sessionStorage.setItem("nom", nom);
		sessionStorage.setItem("prenom", prenom);
		sessionStorage.setItem("telephone", telephone);
		sessionStorage.setItem("email", email);
		sessionStorage.setItem("npass", npass);
		sessionStorage.setItem("pexp", pexp);
		sessionStorage.setItem("motif", motif);
		sessionStorage.setItem("depart", depart);
		sessionStorage.setItem("nbord", nbord);
		sessionStorage.setItem("banq", banq);
		sessionStorage.setItem("assur", assur);
		sessionStorage.setItem("compass", compass);
		sessionStorage.setItem("drdv", drdv);
		sessionStorage.setItem("hrdv", hrdv);
		sessionStorage.setItem("etat", etat);
	},
	
	readinfosperso: function () {
		var id = sessionStorage.getItem("id"),
		    nom = sessionStorage.getItem("nom"),
		    prenom = sessionStorage.getItem("prenom"),
		    telephone = sessionStorage.getItem("telephone"),
		    email = sessionStorage.getItem("email"),
		    npass = sessionStorage.getItem("npass"),
		    pexp = sessionStorage.getItem("pexp"),
		    motif = sessionStorage.getItem("motif"),
		    depart = sessionStorage.getItem("depart"),
		    nbord = sessionStorage.getItem("nbord"),
		    banq = sessionStorage.getItem("banq"),
		    assur = sessionStorage.getItem("assur"),
		    compass = sessionStorage.getItem("compass"),
		    etat = sessionStorage.getItem("etat");
		$("#nom").html(nom);
		$("#prenom").html(prenom);
		$("#telephone").html(telephone);
		$("#email").html(email);
		$("#npass").html(npass);
		$("#pexp").html(pexp);
		$("#motif").html(motif);
		$("#depart").html(depart);
		$("#nbord").html(nbord);
		$("#banq").html(banq);
		$("#assur").html(assur);
		$("#compass").html(compass);
		$("#etat").html(etat);
	},
	
	readrdvinfos: function () {
		var id = sessionStorage.getItem("id"),
		    drdv = sessionStorage.getItem("drdv"),
		    hrdv = sessionStorage.getItem("hrdv"),
		    etat = sessionStorage.getItem("etat");

		$("#drdv").html(drdv);
		$("#hrdv").html(hrdv);
		$("#etat").html(etat);
	},
	
	removeinfos: function () {
		sessionStorage.removeItem("id");
		sessionStorage.removeItem("nom");
		sessionStorage.removeItem("prenom");
		sessionStorage.removeItem("telephone");
		sessionStorage.removeItem("email");
		sessionStorage.removeItem("npass");
		sessionStorage.removeItem("pexp");
		sessionStorage.removeItem("motif");
		sessionStorage.removeItem("depart");
		sessionStorage.removeItem("nbord");
		sessionStorage.removeItem("banq");
		sessionStorage.removeItem("assur");
		sessionStorage.removeItem("compass");
		sessionStorage.removeItem("drdv");
		sessionStorage.removeItem("hrdv");
		sessionStorage.removeItem("etat");
	},
	
	hf: $(document).on('pagebeforecreate', '#index', '#proc', function () {
        $('<div>').attr({'data-role': 'header', 'data-theme': 'b', 'data-position': 'fixed', 'data-id': 'footer', 'data-tap-toggle': 'false'}).append('<h1>MON RDV VISA</h1>').appendTo($(this));
        $('<div>').attr({'data-role': 'footer', 'data-theme': 'b', 'data-position': 'fixed', 'data-id': 'footer', 'data-tap-toggle': 'false'}).append(
            '<div data-role="navbar" data-grid="c">' +
                '<ul><li><a href="perso.html" data-ajax="false" data-transition="slide"><b class="icon-user icon-2x"></b><div style="font-size:9px">MES INFO</div></a></li>' +
                '<li><a href="rdv.html" data-ajax="false" data-transition="slide"><b class="icon-time icon-2x"></b><div style="font-size:9px">MON RDV</div></a></li>' +
                '<li><a href="consult.html" data-ajax="false" data-transition="slide"><b class="icon-search icon-2x" onclick"app.setinfos()"></b><div style="font-size:9px">CONSULTER</div></a></li>' +
                ' <li><a href="index.html" data-ajax="false" data-transition="slide"><b class="icon-th icon-2x"></b><div style="font-size:9px">MENU</div></a></li>' +
                '</ul></div>'
        ).appendTo($(this));
		app.initialize();
	}),
	
	setinfos: function () {
		$.getJSON('http://webcoom.net/app/user.php',
             function (json) {
			    app.writeinfos(json.id, json.nom, json.prenom, json.telephone, json.email, json.npass, json.pexp, json.motif, json.depart, json.nbord, json.banq, json.assur, json.compass, json.drdv, json.hrdv, json.etat);
		    });
	},
	
	loadinfos: function () {
		alert('fgjh jghkjf jkh ');
		$.getJSON('http://webcoom.net/app/user.php',
		    function (json) {
			    app.writeinfos(json.id, json.nom, json.prenom, json.telephone, json.email, json.npass, json.pexp, json.motif, json.depart, json.nbord, json.banq, json.assur, json.compass, json.drdv, json.hrdv, json.etat);
		    });
	},
	
	consult: function (pass, bord) {
        var postData = $(this).serialize();
        $.ajax({
            type: 'GET',
            url: 'http://webcoom.net/app/xl.php?pass=' + pass + '&bord=' + bord +'',
            dataType: 'JSON',
            crossDomain: true,
            timeout: 5000,
            success: function (jsonp) {
                $.each(jsonp, function (i, item) {
                    $("ul#rdv").append('<li><a>' + item.nom + '</a></li>');
                    $("#index").trigger('create');
                });
            },
            error: function (data) {
                $("ul#rdv").append('<li>There was an error loading the bugs');
            }
        });
	},
	
	addmenu: function () {
//	    $.getJSON('http://webcoom.net/app/menu.php',
//		    function (json) {
//		        $.each(json, function (i, item) {
//                    var $menu = '<div class="' + item.bloc + '"><div class="ui-bar">' +
//                                '<a href="' + item.href + '" data-theme="b" data-ajax="false" data-transition="slide">' +
//                                '<b class="' + item.icon + ' icon-2x"></b><div style="font-size:9px">' + item.name + '</div></a></div>' +
//                                '</div>';
//                    $('.ui-grid-b').append($menu);
//                    $('#index').trigger('create');
//                });
//            });
             $.ajax({
            url: 'http://webcoom.net/app/menu.php?callback=?',
            cache: false,
            type: 'GET',
            dataType: "jsonp",
            timeout: 200,
            crossDomain: true,
            jsonp: 'jsonp_callback',
            success: function (data, status) {
                aler('sdf d shjh jkh');
            },
            error: function (xOptions, textStatus) {
                mySurvey.closePopup();
            }
            });
	},
	
	refreshinfos: function () {
		app.removeinfos();
		app.setinfos();
		$.mobile.navigate("./perso.html");
	},
	
	href: function (href) {
		$.mobile.navigate(href);
		
	}

};
